
package com.crmpoc.customer;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.crmpoc.customer package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.crmpoc.customer
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateAddressRequest }
     * 
     */
    public CreateAddressRequest createCreateAddressRequest() {
        return new CreateAddressRequest();
    }

    /**
     * Create an instance of {@link AddressDetail }
     * 
     */
    public AddressDetail createAddressDetail() {
        return new AddressDetail();
    }

    /**
     * Create an instance of {@link GetAllAddressResponse }
     * 
     */
    public GetAllAddressResponse createGetAllAddressResponse() {
        return new GetAllAddressResponse();
    }

    /**
     * Create an instance of {@link CreateCustomerRequest }
     * 
     */
    public CreateCustomerRequest createCreateCustomerRequest() {
        return new CreateCustomerRequest();
    }

    /**
     * Create an instance of {@link CustomerDetails }
     * 
     */
    public CustomerDetails createCustomerDetails() {
        return new CustomerDetails();
    }

    /**
     * Create an instance of {@link UpdateAddressRequest }
     * 
     */
    public UpdateAddressRequest createUpdateAddressRequest() {
        return new UpdateAddressRequest();
    }

    /**
     * Create an instance of {@link CustomerValidationRequest }
     * 
     */
    public CustomerValidationRequest createCustomerValidationRequest() {
        return new CustomerValidationRequest();
    }

    /**
     * Create an instance of {@link GetCustomerDetailsRequest }
     * 
     */
    public GetCustomerDetailsRequest createGetCustomerDetailsRequest() {
        return new GetCustomerDetailsRequest();
    }

    /**
     * Create an instance of {@link GetAllAddressRequest }
     * 
     */
    public GetAllAddressRequest createGetAllAddressRequest() {
        return new GetAllAddressRequest();
    }

    /**
     * Create an instance of {@link GetAllCustomerResponse }
     * 
     */
    public GetAllCustomerResponse createGetAllCustomerResponse() {
        return new GetAllCustomerResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomerRequest }
     * 
     */
    public UpdateCustomerRequest createUpdateCustomerRequest() {
        return new UpdateCustomerRequest();
    }

    /**
     * Create an instance of {@link GetCustomerDetailsResponse }
     * 
     */
    public GetCustomerDetailsResponse createGetCustomerDetailsResponse() {
        return new GetCustomerDetailsResponse();
    }

    /**
     * Create an instance of {@link CustomerValidationResponse }
     * 
     */
    public CustomerValidationResponse createCustomerValidationResponse() {
        return new CustomerValidationResponse();
    }

    /**
     * Create an instance of {@link GetAddressDetailsRequest }
     * 
     */
    public GetAddressDetailsRequest createGetAddressDetailsRequest() {
        return new GetAddressDetailsRequest();
    }

    /**
     * Create an instance of {@link GetAddressDetailsResponse }
     * 
     */
    public GetAddressDetailsResponse createGetAddressDetailsResponse() {
        return new GetAddressDetailsResponse();
    }

    /**
     * Create an instance of {@link GetAllCustomerRequest }
     * 
     */
    public GetAllCustomerRequest createGetAllCustomerRequest() {
        return new GetAllCustomerRequest();
    }

}
