//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.01.21 at 02:34:56 PM EET 
//


package com.crmpoc.catalog;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.crmpoc.catalog package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.crmpoc.catalog
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OrderFulfilmentRequest }
     * 
     */
    public OrderFulfilmentRequest createOrderFulfilmentRequest() {
        return new OrderFulfilmentRequest();
    }

    /**
     * Create an instance of {@link GetAllProductResponse }
     * 
     */
    public GetAllProductResponse createGetAllProductResponse() {
        return new GetAllProductResponse();
    }

    /**
     * Create an instance of {@link ProductDetails }
     * 
     */
    public ProductDetails createProductDetails() {
        return new ProductDetails();
    }

    /**
     * Create an instance of {@link GetAllProductRequest }
     * 
     */
    public GetAllProductRequest createGetAllProductRequest() {
        return new GetAllProductRequest();
    }

    /**
     * Create an instance of {@link OrderFulfilmentResponse }
     * 
     */
    public OrderFulfilmentResponse createOrderFulfilmentResponse() {
        return new OrderFulfilmentResponse();
    }

    /**
     * Create an instance of {@link GetProductDetailsResponse }
     * 
     */
    public GetProductDetailsResponse createGetProductDetailsResponse() {
        return new GetProductDetailsResponse();
    }

    /**
     * Create an instance of {@link UpdateProductRequest }
     * 
     */
    public UpdateProductRequest createUpdateProductRequest() {
        return new UpdateProductRequest();
    }

    /**
     * Create an instance of {@link GetProductDetailsRequest }
     * 
     */
    public GetProductDetailsRequest createGetProductDetailsRequest() {
        return new GetProductDetailsRequest();
    }

    /**
     * Create an instance of {@link SaveProductRequest }
     * 
     */
    public SaveProductRequest createSaveProductRequest() {
        return new SaveProductRequest();
    }

}
